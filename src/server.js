const Koa = require('koa')
const cors = require('@koa/cors')
const app = new Koa()

app.use(cors())

require('dotenv').config()

app.context.env = process.env

require('./store/mongo')

app.use(require('./routes/accounts').routes())
app.use(require('./routes/admin').routes())
app.use(require('./routes/client').routes())

app.listen(process.env.API_PORT, () => {
  const message = `Server listening on http://127.0.0.1:${process.env.API_PORT}`

  console.log(message)
})
