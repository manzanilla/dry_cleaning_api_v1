const mongoose = require('mongoose')
const { ObjectId } = mongoose.Schema.Types

const DOC_FIELD = {}

DOC_FIELD.STATUS = {}
DOC_FIELD.STATUS.NEW = 0
DOC_FIELD.STATUS.IN_PROCESSING = 1
DOC_FIELD.STATUS.READY = 2
DOC_FIELD.STATUS.REFUND = 3
DOC_FIELD.STATUS.DONE = 4
DOC_FIELD.STATUS.COMPLETED = 5

const OrdersSchema = new mongoose.Schema({
  amount: { type: Number, required: true },
  seller: { type: ObjectId, ref: 'Accounts', required: true },
  customer: { type: ObjectId, ref: 'Accounts', required: true },
  customerName: { type: String },
  creationDate: { type: Date, default: Date.now },
  service: { type: ObjectId, ref: 'Services', required: true },
  sellerStatus: {
    type: Number,
    enum: Object.values(DOC_FIELD.STATUS),
    default: DOC_FIELD.STATUS.NEW
  },
  customerStatus: {
    type: Number,
    enum: Object.values(DOC_FIELD.STATUS),
    default: DOC_FIELD.STATUS.IN_PROCESSING
  },
  refundReason: String
}, { timestamps: true })

module.exports = { OrdersSchema, DOC_FIELD }
