const mongoose = require('mongoose')
const { validateEmail } = require('../helpers/validateUtil')
const DOC_FIELD = {}

DOC_FIELD.ROLE = {}
DOC_FIELD.ROLE.CLIENT = 0
DOC_FIELD.ROLE.ADMIN = 27

const AccountsSchema = new mongoose.Schema({
  firstName: { type: String, default: '' },
  surname: { type: String, default: '' },
  patronymic: { type: String, default: '' },
  balance: { type: Number },
  email: {
    type: String,
    required: true,
    unique: true,
    trim: true,
    lowercase: true,
    validate: validateEmail
  },
  salt: { type: String, required: true },
  hash: { type: String, required: true },
  role: {
    type: Number,
    enum: Object.values(DOC_FIELD.ROLE),
    required: true
  },
  refreshToken: String,
  emailCode: String,
  restorePasswordToken: String
}, {
  timestamps: true
})

module.exports = {
  AccountsSchema,
  DOC_FIELD
}
