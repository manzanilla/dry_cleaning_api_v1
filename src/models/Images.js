const mongoose = require('mongoose')
const { ObjectId } = mongoose.Schema.Types

const ImagesSchema = new mongoose.Schema({
  path: { type: String , required: true},
  uploadedBy: { type: ObjectId, ref: 'Accounts', required: true }
}, {
  timestamps: true
})

module.exports = { ImagesSchema }
