const mongoose = require('mongoose')
const { ObjectId } = mongoose.Schema.Types

const DOC_FIELD = {}

DOC_FIELD.STATUS = {}
DOC_FIELD.STATUS.CREATED = 0
DOC_FIELD.STATUS.DELETED = 1

const ServicesSchema = new mongoose.Schema({
  name: {
    type: String,
    minLength: 3,
    maxLength: 128
  },
  price: {
    type: Number,
    required: true,
    min: 1
  },
  owner: { type: ObjectId, ref: 'Accounts', required: true },
  department: { type: ObjectId, ref: 'Departments', required: true },
  status: {
    type: Number,
    enum: Object.values(DOC_FIELD.STATUS),
    default: DOC_FIELD.STATUS.CREATED
  }
}, {
  timestamps: true
})

module.exports = { ServicesSchema, DOC_FIELD }
