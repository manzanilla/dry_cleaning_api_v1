const mongoose = require('mongoose')
const { ObjectId } = mongoose.Schema.Types

const DOC_FIELD = {}

DOC_FIELD.STATUS = {}
DOC_FIELD.STATUS.CREATED = 0
DOC_FIELD.STATUS.DELETED = 1

const GallerySchema = new mongoose.Schema({
  mimetype: {
    type: String,
    enum: ['image/png', 'image/jpeg', 'image/webp'],
    required: true
  },
  url: {
    // @todo validate url
    type: String,
    required: true
  },
  size: { type: Number, required: true },
  isAvatar: { type: Boolean }
}, {
  timestamps: true
})

const DepartmentsSchema = new mongoose.Schema({
  name: String,
  description: String,
  owner: { type: ObjectId, ref: 'Accounts', required: true },
  services: [{ type: ObjectId, ref: 'Services' }],
  avatar: String,
  gallery: [ GallerySchema ],
  status: {
    type: Number,
    enum: Object.values(DOC_FIELD.STATUS),
    default: DOC_FIELD.STATUS.CREATED
  }
}, {
  timestamps: true
})

module.exports = { DepartmentsSchema, DOC_FIELD }
