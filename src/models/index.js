const { model } = require('mongoose')

const { AccountsSchema } = require('./Accounts')
const { DepartmentsSchema } = require('./Departments')
const { ServicesSchema } = require('./Services')
const { ImagesSchema } = require('./Images')
const { OrdersSchema } = require('./Orders')

module.exports = {
  Accounts: model('Accounts', AccountsSchema),
  Departments: model('Departments', DepartmentsSchema),
  Images: model('Images', ImagesSchema),
  Services: model('Services', ServicesSchema),
  Orders: model('Orders', OrdersSchema)
}
