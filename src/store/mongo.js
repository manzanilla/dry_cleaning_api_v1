let uri = `mongodb://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}`
uri += `@localhost:27017/${process.env.MONGO_DB_NAME}?authSource=admin`

const mongoose = require('mongoose')

mongoose.connect(uri, {
  useUnifiedTopology: true,
  useNewUrlParser: true
}, (error) => {
  if (error) console.error(error)
})

mongoose.connection.once('open', () => {
  console.log('MongoDB database connection established successfully')
})
