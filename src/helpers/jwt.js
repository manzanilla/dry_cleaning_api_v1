const jwt = require('jsonwebtoken')

const sign = (accountId, accountRole) => {
  const payload = { accountId, accountRole }
  const options = {
    token: { expiresIn: process.env.ACCESS_TOKEN_EXPIRES_IN },
    refreshToken:  { expiresIn: process.env.REFRESH_TOKEN_EXPIRES_IN }
  }
  const accessToken = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, options.token)
  const refreshToken = jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET,  options.refreshToken)

  return { accessToken, refreshToken }
}

const signRestorePassword = (accountId) => {
  const payload = { accountId }
  const options = { expiresIn: process.env.RESTORE_PASSWORD_TOKEN_EXPIRES_IN }

  return jwt.sign(payload, process.env.RESTORE_PASSWORD_TOKEN_SECRET, options)
}

module.exports = {
  sign,
  signRestorePassword
}
