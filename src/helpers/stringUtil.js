const shuffle = (a) => {
  let j, x, i

  for (i = a.length - 1; i > 0; i--) {
    j = Math.floor(Math.random() * (i + 1))
    x = a[i]
    a[i] = a[j]
    a[j] = x
  }

  return a
}

const getRandomString = (params = {}) => {
  const {
    numericDigits = true,
    uppercaseLetters = true,
    lowercaseLetters = true,
    length = 10
  } = params

  let characters = ''

  if (numericDigits) {
    characters += '0123456789'
  }

  if (lowercaseLetters) {
    characters += 'abcdefghijklmnopqrstuvwxyz'
  }

  if (uppercaseLetters) {
    characters += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
  }

  return shuffle(characters.split('')).slice(0, length).join('')
}

module.exports = {
  getRandomString
}
