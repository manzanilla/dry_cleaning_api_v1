const router = require('koa-router')({
  prefix: '/accounts',
  sensitive: true
})
const koaBody = require('koa-body')
const accountController = require('../controllers/accounts.controller')

router.post('/signUp', koaBody(), accountController.signUp)
router.post('/signIn', koaBody(), accountController.signIn)
router.post('/refreshToken', koaBody(), accountController.refreshToken)

router.post('/restorePasswordToken', koaBody(), accountController.restorePasswordToken)
router.post('/restorePassword', koaBody(), accountController.restorePassword)

module.exports = router
