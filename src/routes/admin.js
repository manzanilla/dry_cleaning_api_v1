const compose = require('koa-compose')
const router = require('koa-router')({
  prefix: '/admin',
  sensitive: true
})
const jwtVerifyMiddleware = require('../middlewares/jwtVerify.middleware')
const isAdminMiddleware = require('../middlewares/isAdmin.middleware')
const accessMiddlewares = compose([jwtVerifyMiddleware, isAdminMiddleware])
const koaBody = require('koa-body')
const adminController = require('../controllers/admin.controller')

router.get('/ping', accessMiddlewares, adminController.ping)

router.post('/department', accessMiddlewares, koaBody(), adminController.createDepartment)
router.get('/department/:departmentId', accessMiddlewares, adminController.getDepartmentById)
router.get('/departments', accessMiddlewares, adminController.getDepartmentList)
router.put('/department/:departmentId', accessMiddlewares, koaBody(), adminController.updateDepartmentById)
router.post('/department/:departmentId/gallery', accessMiddlewares, koaBody(), adminController.uploadDepartmentPhotos)
router.put(
  '/department/:departmentId/gallery/:galleryId/avatar',
  accessMiddlewares,
  koaBody(),
  adminController.setDepartmentGalleryAvatar
)
router.delete('/department/:departmentId', accessMiddlewares, adminController.deleteDepartmentById)

router.post('/department/:departmentId/service', accessMiddlewares, koaBody(), adminController.createService)
router.get('/service/:serviceId', accessMiddlewares, adminController.getServiceById)
router.get('/department/:departmentId/services', accessMiddlewares, adminController.getServiceListByDepartmentId)
router.put('/service/:serviceId', accessMiddlewares, koaBody(), adminController.updateServiceById)
router.delete('/service/:serviceId', accessMiddlewares, koaBody(), adminController.deleteServiceById)

router.get('/orders', accessMiddlewares, adminController.getOrderList)
router.put('/order/:orderId', accessMiddlewares, adminController.updateOrderById)

module.exports = router
