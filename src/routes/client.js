const compose = require('koa-compose')
const router = require('koa-router')({
  prefix: '/client',
  sensitive: true
})

const jwtVerifyMiddleware = require('../middlewares/jwtVerify.middleware')
const isClientMiddleware = require('../middlewares/isClient.middleware')
const accessMiddlewares = compose([jwtVerifyMiddleware, isClientMiddleware])
const koaBody = require('koa-body')
const clientController = require('../controllers/client.controller')

router.get('/ping', accessMiddlewares, clientController.pong)

router.get('/departments', accessMiddlewares, clientController.getDepartmentList)
router.get('/department/:departmentId/services', accessMiddlewares, clientController.getServiceListByDepartmentId)
router.post('/order', accessMiddlewares, koaBody(), clientController.createOrder)
router.get('/orders', accessMiddlewares, clientController.getOrderList)

module.exports = router
