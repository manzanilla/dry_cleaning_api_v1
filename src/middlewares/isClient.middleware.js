const { DOC_FIELD: ACCOUNTS_DOC_FIELD } = require('./../models/Accounts')

module.exports = async (ctx, next) => {
  try {
    const { accountRole } = ctx.request.jwtPayload

    if(accountRole !== ACCOUNTS_DOC_FIELD.ROLE.CLIENT) {
      ctx.status = 403

      return
    }
  } catch (err) {
    ctx.status = 403

    return
  }

  await next()
}
