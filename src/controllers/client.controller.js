const { DOC_FIELD: DEPARTMENTS_DOC_FIELD } = require('../models/Departments')
const { DOC_FIELD: SERVICES_DOC_FIELD } = require('../models/Services')
const { DOC_FIELD: ORDERS_DOC_FIELD } = require('../models/Orders')
const { Departments, Services, Orders, Accounts } = require('../models')

const pong = (ctx) => {
  ctx.body = 'pong client'
}

const getDepartmentList = async (ctx) => {
  try {
    let { limit, offset } = ctx.request.query

    limit = limit ? +limit : 10
    offset = offset ? +offset : 0

    const filter = { status: DEPARTMENTS_DOC_FIELD.STATUS.CREATED }

    const totalCount = await Departments.countDocuments(filter)

    if (!totalCount) {
      ctx.body = { totalCount }

      return
    }

    const departments = await Departments
      .find(filter)
      .select('-services -owner -status -createdAt -__v -updatedAt -images')
      .sort({ createdAt: -1 })
      .skip(offset)
      .limit(limit)
      .lean()

    ctx.body = { totalCount, departments }
  } catch (e) {
    onCatch(ctx, e)
  }
}

const getServiceListByDepartmentId = async (ctx) => {
  try {
    const { departmentId } = ctx.params
    let { limit, offset } = ctx.request.query

    limit = limit ? +limit : 10
    offset = offset ? +offset : 0

    const filter = {
      department: departmentId,
      status: SERVICES_DOC_FIELD.STATUS.CREATED
    }

    const totalCount = await Services.countDocuments(filter)

    if (!totalCount) {
      ctx.body = { totalCount }

      return
    }

    const services = await Services
      .find(filter)
      .select('-__v -createdAt -updatedAt -createdAt -status -department -owner')
      .sort({ createdAt: -1 })
      .skip(offset)
      .limit(limit)
      .lean()

    ctx.body = { totalCount, services }
  } catch (e) {
    onCatch(ctx, e)
  }
}

// @todo transaction
const createOrder = async (ctx) => {
  try {
    const { serviceId } = ctx.request.body
    const service = await Services.findOne({
      _id: serviceId,
      status: SERVICES_DOC_FIELD.STATUS.CREATED
    }).lean()

    if (!service) {
      ctx.status = 400
      ctx.body = {
        message: 'Service not found'
      }

      return
    }

    const { accountId: customer } = ctx.request.jwtPayload
    const customerAccount = await Accounts.findById(customer)

    if (!customerAccount) {
      ctx.status = 400
      ctx.body = {
        message: 'Customer not found'
      }

      return
    }

    const { owner: seller, price: amount } = service

    if (customerAccount.balance < amount) {
      ctx.status = 400
      ctx.body = {
        message: 'Not enough funds'
      }

      return
    }

    const sellerStatus = ORDERS_DOC_FIELD.STATUS.NEW
    const customerStatus = ORDERS_DOC_FIELD.STATUS.IN_PROCESSING
    const order = await Orders.create({
      amount,
      customerName: `${customerAccount.firstName} ${customerAccount.surname} ${customerAccount.patronymic}`,
      seller,
      customer,
      service,
      sellerStatus,
      customerStatus
    })

    customerAccount.balance -= amount
    await customerAccount.save()

    ctx.body = order
  } catch (e) {
    onCatch(ctx, e)
  }
}

const getOrderList = async (ctx) => {
  try {
    const { accountId: customer } = ctx.request.jwtPayload
    const filter = { customer }
    let { limit, offset } = ctx.request.query

    limit = limit ? +limit : 10
    offset = offset ? +offset : 0

    const totalCount = await Orders.countDocuments(filter)

    if (!totalCount) {
      ctx.body = { totalCount }

      return
    }

    const orders = await Orders
      .find(filter)
      .select('_id amount seller customerStatus')
      .skip(offset)
      .limit(limit)
      .lean()

    ctx.body = { totalCount, orders }
  } catch (e) {
    onCatch(ctx, e)
  }
}

const onCatch = (ctx, e) => {
  console.error(e)

  ctx.status = 400
  ctx.body = { message: e.toString() }
}

module.exports = {
  pong,
  getDepartmentList,
  getServiceListByDepartmentId,
  createOrder,
  getOrderList
}
