const { DOC_FIELD: DEPARTMENTS_DOC_FIELD } = require('./../models/Departments')
const { DOC_FIELD: SERVICES_DOC_FIELD } = require('./../models/Services')
const { DOC_FIELD: ORDERS_DOC_FIELD } = require('./../models/Orders')
const { Departments, Services, Orders, Accounts } = require('./../models')

const ping = (ctx) => {
  ctx.body = 'pong admin'
}

const createDepartment = async (ctx) => {
  try {
    const { name = '', description = '' } = ctx.request.body
    const { accountId: owner } = ctx.request.jwtPayload

    ctx.body = await Departments.create({
      name,
      description,
      owner
    })
  } catch (e) {
    onCatch(ctx, e)
  }
}

const getDepartmentList = async (ctx) => {
  try {
    const { accountId: owner } = ctx.request.jwtPayload
    let { limit, offset } = ctx.request.query

    limit = limit ? +limit : 10
    offset = offset ? +offset : 0

    const filter = {
      owner,
      status: DEPARTMENTS_DOC_FIELD.STATUS.CREATED
    }

    const totalCount = await Departments.countDocuments(filter)

    if (!totalCount) {
      ctx.body = { totalCount }

      return
    }

    const departments = await Departments
      .find(filter)
      .sort({ createdAt: -1 })
      .skip(offset)
      .limit(limit)
      .lean()

    ctx.body = { totalCount, departments }
  } catch (e) {
    onCatch(ctx, e)
  }
}

const getDepartmentById = async (ctx) => {
  try {
    const { departmentId } = ctx.params
    const { accountId: owner } = ctx.request.jwtPayload
    const filter = {
      _id: departmentId,
      owner,
      status: DEPARTMENTS_DOC_FIELD.STATUS.CREATED
    }
    const department = await Departments.findOne(filter).lean()

    if (!department) {
      ctx.status = 404

      return
    }

    ctx.body = department
  } catch (e) {
    onCatch(ctx, e)
  }
}

const updateDepartmentById = async (ctx) => {
  try {
    const { departmentId } = ctx.params
    const { accountId: owner } = ctx.request.jwtPayload
    const filter = {
      _id: departmentId,
      owner,
      status: DEPARTMENTS_DOC_FIELD.STATUS.CREATED
    }
    const department = await Departments.findOne(filter)

    if (!department) {
      ctx.status = 404

      return
    }

    const {
      name,
      description
    } = ctx.request.body

    if (name) department.name = name
    if (description) department.description = description

    await department.save()

    ctx.body = department
  } catch (e) {
    onCatch(ctx, e)
  }
}

const deleteDepartmentById = async (ctx) => {
  try {
    const { departmentId } = ctx.params
    const { accountId: owner } = ctx.request.jwtPayload
    const filter = {
      _id: departmentId,
      owner,
      status: DEPARTMENTS_DOC_FIELD.STATUS.CREATED
    }
    const update = { status: DEPARTMENTS_DOC_FIELD.STATUS.DELETED }
    const options = { new: true }

    const department = await Departments.findOneAndUpdate(filter, update, options)

    if (!department) {
      ctx.status = 404

      return
    }

    ctx.body = department
  } catch (e) {
    onCatch(ctx, e)
  }
}

const createService = async (ctx) => {
  try {
    const { departmentId } = ctx.params
    const { accountId: owner } = ctx.request.jwtPayload
    const filter = {
      _id: departmentId,
      owner,
      status: DEPARTMENTS_DOC_FIELD.STATUS.CREATED
    }
    const department = await Departments.findOne(filter)

    if (!department) {
      ctx.status = 404

      return
    }

    const { name = '', price } = ctx.request.body
    const doc = { owner, name, price, department: departmentId }
    const service = await Services.create(doc)
    await department.update( { $push: { services: service._id } } )

    ctx.body = service
  } catch (e) {
    onCatch(ctx, e)
  }
}

const getServiceById = async (ctx) => {
  try {
    const { serviceId } = ctx.params
    const { accountId: owner } = ctx.request.jwtPayload
    const filter = {
      _id: serviceId,
      owner,
      status: SERVICES_DOC_FIELD.STATUS.CREATED
    }

    const service = await Services
      .findOne(filter)
      .lean()

    if (!service) {
      ctx.status = 404

      return
    }

    ctx.body = service
  } catch (e) {
    onCatch(ctx, e)
  }
}

const getServiceListByDepartmentId = async (ctx) => {
  try {
    const { accountId: owner } = ctx.request.jwtPayload
    const { departmentId } = ctx.params
    let { limit, offset } = ctx.request.query

    limit = limit ? +limit : 10
    offset = offset ? +offset : 0

    const filter = {
      owner,
      department: departmentId,
      status: SERVICES_DOC_FIELD.STATUS.CREATED
    }

    const totalCount = await Services.countDocuments(filter)

    if (!totalCount) {
      ctx.body = { totalCount }

      return
    }

    const services = await Services
      .find(filter)
      .sort({ createdAt: -1 })
      .skip(offset)
      .limit(limit)
      .lean()

    ctx.body = { totalCount, services }
  } catch (e) {
    onCatch(ctx, e)
  }
}

const updateServiceById = async (ctx) => {
  try {
    const { serviceId } = ctx.params
    const { accountId: owner } = ctx.request.jwtPayload
    const filter = {
      _id: serviceId,
      owner,
      status: SERVICES_DOC_FIELD.STATUS.CREATED
    }
    const service = await Services.findOne(filter)

    if (!service) {
      ctx.status = 404

      return
    }

    const {
      name,
      price
    } = ctx.request.body

    if (name) service.name = name
    if (price) service.price = price

    await service.save()

    ctx.body = service
  } catch (e) {
    onCatch(ctx, e)
  }
}

const deleteServiceById = async (ctx) => {
  try {
    const { serviceId } = ctx.params
    const { accountId: owner } = ctx.request.jwtPayload
    const filter = {
      _id: serviceId,
      owner,
      status: SERVICES_DOC_FIELD.STATUS.CREATED
    }
    const update = { status: SERVICES_DOC_FIELD.STATUS.DELETED }
    const options = { new: true }

    const service = await Services.findOneAndUpdate(filter, update, options)

    if (!service) {
      ctx.status = 404

      return
    }

    ctx.body = service
  } catch (e) {
    onCatch(ctx, e)
  }
}

const getOrderList = async (ctx) => {
  try {
    const { accountId: seller } = ctx.request.jwtPayload
    const filter = { seller }
    let { limit, offset } = ctx.request.query

    limit = limit ? +limit : 10
    offset = offset ? +offset : 0

    const totalCount = await Orders.countDocuments(filter)

    if (!totalCount) {
      ctx.body = { totalCount }

      return
    }

    const orders = await Orders
      .find(filter)
      .sort({ updatedAt: -1 })
      .skip(offset)
      .limit(limit)
      .lean()

    ctx.body = { totalCount, orders }
  } catch (e) {
    onCatch(ctx, e)
  }
}

// @todo transaction
const updateOrderById = async (ctx) => {
  try {
    const { orderId } = ctx.params
    const {
      amount,
      customerName,
      service,
      status,
      creationDate,
      refundReason
    } = ctx.request.body

    const { accountId: seller } = ctx.request.jwtPayload
    const filter = { _id: orderId, seller }
    const order = await Orders.findOne(filter)

    if (!order) {
      ctx.status = 400
      ctx.body = {
        message: 'Order not found'
      }

      return
    }

    if (order.sellerStatus === ORDERS_DOC_FIELD.STATUS.REFUND) {
      ctx.status = 403

      return
    }

    const customer = await Accounts.findById(order.customer)

    if (!customer) {
      ctx.status = 400
      ctx.body = {
        message: 'Customer not found'
      }

      return
    }

    if (service) {
      const filter = { _id: service, owner: seller }
      const serviceDoc = await Services.findOne(filter)

      if (!serviceDoc) {
        ctx.status = 400
        ctx.body = {
          message: 'Service not found'
        }

        return
      }

      order.service = service
    }

    if (amount) order.amount = amount
    if (customerName) order.customerName = customerName
    if (creationDate) order.creationDate = creationDate

    if (status === ORDERS_DOC_FIELD.STATUS.READY) {
      order.sellerStatus = ORDERS_DOC_FIELD.STATUS.READY
      order.customerStatus = ORDERS_DOC_FIELD.STATUS.DONE
    } else if (status === ORDERS_DOC_FIELD.STATUS.REFUND) {
      order.sellerStatus = ORDERS_DOC_FIELD.STATUS.REFUND
      order.customerStatus = ORDERS_DOC_FIELD.STATUS.REFUND

      customer.balance += order.amount

      await customer.save()

      if (refundReason) {
        order.refundReason = refundReason
      }
    }

    await order.save()

    ctx.body = order
  } catch (e) {
    onCatch(ctx, e)
  }
}

const uploadDepartmentPhotos = async (ctx) => {
  try {
    const { accountId: owner } = ctx.request.jwtPayload
    const { departmentId } = ctx.params
    const filter = {
      _id: departmentId,
      owner,
      status: DEPARTMENTS_DOC_FIELD.STATUS.CREATED
    }
    const department = await Departments.findOne(filter)

    if (!department) {
      ctx.status = 400
      ctx.body = {
        message: 'Department not found'
      }

      return
    }

    const { body: photos = [] } = ctx.request

    await department.update({
      $push: {
        gallery: photos
      }
    }, { upsert: true, new: true })

    ctx.body = department
  } catch (e) {
    onCatch(ctx, e)
  }
}

// @todo refactor
const setDepartmentGalleryAvatar = async (ctx) => {
  try {
    const { accountId: owner } = ctx.request.jwtPayload
    const { departmentId } = ctx.params
    const { galleryId } = ctx.params
    const filter = {
      _id: departmentId,
      owner,
      status: DEPARTMENTS_DOC_FIELD.STATUS.CREATED,
      'gallery._id': galleryId
    }
    const department = await Departments.findOne(filter)
      .select('gallery avatar')

    if (!department) {
      ctx.status = 400
      ctx.body = {
        message: 'Department not found'
      }

      return
    }

    const { gallery = [] } = department

    let avatar
    for (const el of gallery) {
      if (el._id.toString() === galleryId) {

        avatar = el.url
        break
      }
    }

    if (avatar) {

      // @todo change url to 120x120
      department.avatar = avatar
      await department.save()
    }

    ctx.body = department
  } catch (e) {
    onCatch(ctx, e)
  }
}

const onCatch = (ctx, e) => {
  console.error(e)

  ctx.status = 400
  ctx.body = { message: e.toString() }
}

module.exports = {
  ping,
  createDepartment,
  getDepartmentById,
  getDepartmentList,
  updateDepartmentById,
  deleteDepartmentById,
  createService,
  getServiceById,
  getServiceListByDepartmentId,
  updateServiceById,
  deleteServiceById,
  getOrderList,
  updateOrderById,
  uploadDepartmentPhotos,
  setDepartmentGalleryAvatar
}
