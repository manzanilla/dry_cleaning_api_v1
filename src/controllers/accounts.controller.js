const { Accounts } = require('./../models')
const { DOC_FIELD: ACCOUNTS_DOC_FIELD } = require('./../models/Accounts')
const { sha512 } = require('./../helpers/passwordUtil')
const { getRandomString } = require('./../helpers/stringUtil')
const jwt = require('jsonwebtoken')
const jwtHelper = require('./../helpers/jwt')

const signUp = async (ctx) => {
  try {
    const {
      firstName = '',
      surname = '',
      patronymic = '',
      email,
      password,
      role
    } = ctx.request.body

    const salt = getRandomString({ length: 16 })
    const emailCode = getRandomString({ length: 16 })
    const { hash } = sha512(password, salt)
    const account = await Accounts.create({
      firstName,
      surname,
      patronymic,
      email,
      emailCode,
      salt,
      hash,
      role
    })

    if (role === ACCOUNTS_DOC_FIELD.ROLE.CLIENT) {
      account.balance = Math.floor(Math.random() * (500 - 100 + 1) +100)
    }

    const { accessToken, refreshToken } = jwtHelper.sign(account._id, role)
    account.refreshToken = refreshToken
    await account.save()

    ctx.body = {
      accessToken,
      refreshToken
    }
  } catch (e) {
    console.error(e)

    ctx.status = 400
    ctx.body = { message: e.toString() }
  }
}

const signIn = async (ctx) => {
  try {
    const { email, password } = ctx.request.body
    const account = await Accounts
      .findOne({ email })

    if (!account) {
      ctx.status = 401

      return
    }

    const { hash } = sha512(password, account.salt)

    if (hash !== account.hash) {
      ctx.status = 401

      return
    }

    const { accessToken, refreshToken } = jwtHelper.sign(account._id, account.role)
    account.refreshToken = refreshToken

    await account.save()

    ctx.body = {
      accessToken,
      refreshToken
    }
  } catch (e) {
    console.error(e)

    ctx.status = 400
    ctx.body = { message: e.toString() }
  }
}

const refreshToken = async (ctx) => {
  try {
    const { token } = ctx.request.body
    let accountId

    try {
      const decode = jwt.verify(token, process.env.REFRESH_TOKEN_SECRET)

      accountId = decode.accountId
    } catch (e) {
      console.error(e)

      ctx.status = 401

      return
    }

    const account = await Accounts.findById(accountId)

    if (account.refreshToken !== token) {
      ctx.status = 401

      return
    }

    const { accessToken, refreshToken } = jwtHelper.sign(account._id, account.role)
    account.refreshToken = refreshToken
    await account.save()

    ctx.body = {
      accessToken,
      refreshToken
    }
  } catch (e) {
    console.error(e)

    ctx.status = 400
    ctx.body = { message: e.toString() }
  }
}

const restorePasswordToken = async (ctx) => {
  try {
    const { email } = ctx.request.body
    const account = await Accounts.findOne({ email })

    if (!account) {
      ctx.status = 404

      return
    }

    const { _id: accountId } = account
    account.restorePasswordToken = jwtHelper.signRestorePassword(accountId)

    // @todo send email

    await account.save()

    ctx.status = 200
  } catch (e) {
    onCatch(ctx, e)
  }
}

const restorePassword = async (ctx) => {
  try {
    const {
      token,
      password,
      passwordRepeat
    } = ctx.request.body

    let payload

    try {
      payload = jwt.verify(token, process.env.RESTORE_PASSWORD_TOKEN_SECRET)
    } catch (e) {
      console.error(e)

      ctx.status = 400

      return
    }

    const { accountId } = payload

    const account = await Accounts.findById(accountId)

    if (!account) {
      ctx.status = 400

      return
    }

    if (token !== account.restorePasswordToken) {
      ctx.status = 400

      return
    }

    if (password !== passwordRepeat) {
      ctx.status = 400

      return
    }

    const salt = getRandomString({ length: 16 })
    const { hash } = sha512(password, salt)

    account.hash = hash
    account.salt = salt
    account.restorePasswordToken = undefined

    await account.save()

    ctx.status = 200
  } catch (e) {
    onCatch(ctx, e)
  }
}

const onCatch = (ctx, e) => {
  console.error(e)

  ctx.status = 400
  ctx.body = { message: e.toString() }
}

module.exports = {
  signUp,
  signIn,
  refreshToken,
  restorePasswordToken,
  restorePassword
}
